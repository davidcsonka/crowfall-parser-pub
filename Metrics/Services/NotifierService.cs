using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Media;
using System.Runtime.InteropServices;
using System.Windows.Threading;

namespace Skills.Services
{
    public class NotifierService
    {
        private const uint WinEventOutOfContext = 0;
        private const uint EventSystemForeground = 3;
        private static readonly Dictionary<long, DispatcherTimer> Timers = new Dictionary<long, DispatcherTimer>();
        private WinEventProc _listener;
        private IntPtr _winHook;

        [DllImport("user32.dll", SetLastError = true)]
        private static extern IntPtr SetWinEventHook(uint eventMin, uint eventMax, IntPtr hmodWinEventProc, WinEventProc lpfnWinEventProc, int idProcess, int idThread, uint dwflags);

        [DllImport("user32.dll")]
        private static extern int UnhookWinEvent(IntPtr hWinEventHook);

        private static void OnWindowActivated(IntPtr hWinEventHook, uint iEvent, IntPtr hWnd, int idObject, int idChild, int dwEventThread, int dwmsEventTime)
        {
            var currentHandle = hWnd.ToInt64();

            if (Timers.ContainsKey(currentHandle))
            {
                Timers[currentHandle].Stop();
                Timers.Remove(currentHandle);
            }

            var crowfallWindows = new List<long>();

            foreach (var process in Process.GetProcessesByName("CrowfallClient"))
            {
                var processHandle = process.MainWindowHandle.ToInt64();

                if (currentHandle != processHandle)
                {
                    if (!Timers.ContainsKey(processHandle))
                    {
                        var timer = new DispatcherTimer {Interval = TimeSpan.FromMinutes(Options.Options.Inst.NotifierTimer)};
                        Timers.Add(processHandle, timer);
                        timer.Start();
                        timer.Tick += (sender, args) =>
                        {
                            new SoundPlayer(AppDomain.CurrentDomain.BaseDirectory + @"\crow.wav").Play();
                            timer.Stop();
                        };
                    }
                }

                crowfallWindows.Add(processHandle);
            }

            var outdatedTimers = new List<long>();
            foreach (var timer in Timers)
            {
                if (!crowfallWindows.Contains(timer.Key))
                    outdatedTimers.Add(timer.Key);
            }

            foreach (var outdatedTimer in outdatedTimers)
            {
                Timers[outdatedTimer].Stop();
                Timers.Remove(outdatedTimer);
            }
        }

        public void StartListeningForWindowChanges()
        {
            _listener = OnWindowActivated;
            _winHook = SetWinEventHook(EventSystemForeground, EventSystemForeground, IntPtr.Zero, _listener, 0, 0, WinEventOutOfContext);
        }

        public void StopListeningForWindowChanges()
        {
            UnhookWinEvent(_winHook);
        }

        private delegate void WinEventProc(IntPtr hWinEventHook, uint iEvent, IntPtr hWnd, int idObject, int idChild, int dwEventThread, int dwmsEventTime);
    }
}
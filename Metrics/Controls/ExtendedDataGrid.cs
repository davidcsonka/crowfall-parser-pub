using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Skills.Controls
{
    public class ExtendedDataGrid : DataGrid
    {
        public ExtendedDataGrid()
        {
            IsReadOnly = true;
            AutoGenerateColumns = false;
            SelectionChanged += (sender, args) => SelectAll();
            Name = "DgSkills";
            HeadersVisibility = DataGridHeadersVisibility.Column;
            RowBackground = Brushes.Transparent;
            HorizontalGridLinesBrush = Brushes.Transparent;
            MouseRightButtonDown += (sender, args) => Focus();
            VerticalGridLinesBrush = Brushes.Transparent;
            CanUserReorderColumns = false;
            CanUserResizeColumns = false;
            ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;

            var contextMenu = new ContextMenu();
            contextMenu.Items.Add(new MenuItem {Header = "Copy", Command = ApplicationCommands.Copy});
            ContextMenu = contextMenu;
        }
    }
}
using System;

namespace Skills.Extensions
{
    public static class Extensions
    {
        public static int CalcPerSecond(this int totalDamage, double duration)
        {
            return (int) (duration.Equal(0) ? 0 : totalDamage / duration * 1000);
        }

        public static bool Equal(this double left, double right)
        {
            return Math.Abs(left - right) < 1e-10;
        }
    }
}
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Skills.Extensions;

namespace Skills.Models
{
    public sealed class Skill : INotifyPropertyChanged
    {
        private int _crits;
        private int _hits;
        private int _perSecond;
        private int _total;

        public Skill(string name, int total, bool critical)
        {
            Name = name;
            Total = total;
            PerSecond = 0;
            Hits = 1;
            Crits = critical ? 1 : 0;
        }

        public Skill(string name, int perSecond, int total, int crits, int hits)
        {
            Name = name;
            Total = total;
            PerSecond = perSecond;
            Hits = hits;
            Crits = crits;
        }

        public string Name { get; }

        public int PerSecond
        {
            get { return _perSecond; }
            private set
            {
                _perSecond = value;
                OnPropertyChanged(nameof(PerSecond));
            }
        }

        public int Total
        {
            get { return _total; }
            private set
            {
                _total = value;
                OnPropertyChanged(nameof(Total));
            }
        }

        public int Hits
        {
            get { return _hits; }
            private set
            {
                _hits = value;
                OnPropertyChanged(nameof(Hits));
            }
        }

        public int Crits
        {
            get { return _crits; }
            private set
            {
                _crits = value;
                OnPropertyChanged(nameof(Crits));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void AddHit(int damage, bool critical)
        {
            Total += damage;
            Hits++;

            if (critical)
                Crits++;
        }

        public void UpdatePerSecond(double duration)
        {
            PerSecond = Total.CalcPerSecond(duration);
        }

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Prism.Mvvm;
using Skills.Extensions;
using Skills.ViewModels;

namespace Skills.Models
{
    public class Fight : BindableBase
    {
        private int _maximumHeal;
        private string _maximumHealName;
        private int _maximumHit;
        private string _maximumHitName;
        public int Index { get; set; }
        public string Name { get; set; }
        public DateTime? Started { get; set; }
        public DateTime? LastAction { get; set; }

        public double Duration => LastAction == null || Started == null ? 0 : (LastAction.Value - Started.Value).TotalMilliseconds;

        public int MaximumHit
        {
            get => _maximumHit;
            set => SetProperty(ref _maximumHit, value);
        }

        public string MaximumHitName
        {
            get => _maximumHitName;
            set => SetProperty(ref _maximumHitName, value);
        }

        public int MaximumHeal
        {
            get => _maximumHeal;
            set => SetProperty(ref _maximumHeal, value);
        }

        public string MaximumHealName
        {
            get => _maximumHealName;
            set => SetProperty(ref _maximumHealName, value);
        }

        public ObservableCollection<Skill> DamageDoneRecords { get; } = new ObservableCollection<Skill>();
        public ObservableCollection<Skill> HealingDoneRecords { get; } = new ObservableCollection<Skill>();

        public ObservableCollection<Skill> DamageReceivedRecords { get; } = new ObservableCollection<Skill>();

        public ObservableCollection<Skill> HealingReceivedRecords { get; } = new ObservableCollection<Skill>();
        public ObservableCollection<Opponent> Opponents { get; } = new ObservableCollection<Opponent>();
        public int Dps => GetTotalRecord(DamageDoneRecords)?.PerSecond ?? 0;
        public int DamageDone => GetTotalRecord(DamageDoneRecords)?.Total ?? 0;
        public int DamageReceived => GetTotalRecord(DamageReceivedRecords)?.Total ?? 0;
        public int Hps => GetTotalRecord(HealingDoneRecords)?.PerSecond ?? 0;
        public int HealingDone => GetTotalRecord(HealingDoneRecords)?.Total ?? 0;
        public int HealingReceived => GetTotalRecord(HealingReceivedRecords)?.Total ?? 0;

        private int Crits
        {
            get
            {
                var damageTotal = GetTotalRecord(DamageDoneRecords);
                var healingTotal = GetTotalRecord(HealingDoneRecords);

                return (damageTotal?.Crits ?? 0) + (healingTotal?.Crits ?? 0);
            }
        }

        private int Hits
        {
            get
            {
                var damageTotal = GetTotalRecord(DamageDoneRecords);
                var healingTotal = GetTotalRecord(HealingDoneRecords);

                return (damageTotal?.Hits ?? 0) + (healingTotal?.Hits ?? 0);
            }
        }

        public double CriticalRate => (double) Hits > 0 ? Crits / (double) Hits * 100 : 0;

        private Skill GetTotalRecord(ICollection<Skill> records)
        {
            return records.FirstOrDefault(x => x.Name == "Total");
        }

        public void UpdatePerSecond()
        {
            UpdatePerSecond(DamageDoneRecords);
            UpdatePerSecond(HealingDoneRecords);
        }

        public void UpdateDamageDone(string skill, string target, int amount, bool critical)
        {
            UpdateSkills(DamageDoneRecords, skill, amount, critical);
            UpdateOpponents(target, amount);
            UpdateMaxHitStats(amount, skill);
        }

        private void UpdateMaxHitStats(int amount, string skill)
        {
            if (amount > MaximumHit)
            {
                MaximumHit = amount;
                MaximumHitName = skill;
            }
        }

        private void UpdateMaxHealStats(int amount, string skill)
        {
            if (amount > MaximumHeal)
            {
                MaximumHeal = amount;
                MaximumHealName = skill;
            }
        }


        private void UpdateOpponents(string target, int amount)
        {
            if (string.IsNullOrEmpty(Name))
            {
                Name = target;
            }
            else if (!Name.Contains(target))
            {
                var newName = Name + "; " + target;
                if (newName.Length < 30)
                    Name = newName;
            }

            var opponent = Opponents.FirstOrDefault(x => x.Name == target);
            if (opponent == null)
                Opponents.Add(new Opponent(target, amount));
            else
                opponent.DamageDone += amount;
        }

        public void UpdateHealingDone(string skill, int amount, bool critical)
        {
            UpdateSkills(HealingDoneRecords, skill, amount, critical);
            UpdateMaxHealStats(amount, skill);
        }

        public void UpdateDamageReceived(string skill, int amount, bool critical)
        {
            UpdateSkills(DamageReceivedRecords, skill, amount, critical);
        }

        public void UpdateHealingReceived(string skill, int amount, bool critical)
        {
            UpdateSkills(HealingReceivedRecords, skill, amount, critical);
        }


        private void UpdatePerSecond(IEnumerable<Skill> skills)
        {
            foreach (var skill in skills)
            {
                skill.UpdatePerSecond(Duration);
            }
        }

        private void UpdateSkills(ICollection<Skill> skills, string name, int damage, bool critical)
        {
            var skill = skills.FirstOrDefault(x => x.Name == name);
            if (skill == null)
            {
                skills.Add(new Skill(name, damage, critical));
            }
            else
            {
                skill.AddHit(damage, critical);
            }

            skill = skills.FirstOrDefault(x => x.Name == "Total");
            skills.Remove(skill);
            var total = skills.Sum(x => x.Total);
            skills.Add(new Skill("Total", total.CalcPerSecond(Duration), total, skills.Sum(x => x.Crits), skills.Sum(x => x.Hits)));

            RaisePropertyChanged(nameof(Dps));
            RaisePropertyChanged(nameof(DamageDone));
            RaisePropertyChanged(nameof(DamageReceived));
            RaisePropertyChanged(nameof(Hps));
            RaisePropertyChanged(nameof(HealingDone));
            RaisePropertyChanged(nameof(HealingReceived));
        }
    }
}
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Skills.Models
{
    public class SkillListModel : INotifyPropertyChanged
    {
        private ObservableCollection<Skill> _skills;

        public ObservableCollection<Skill> Skills
        {
            get { return _skills; }
            set
            {
                _skills = value;
                OnPropertyChanged(nameof(Skills));
            }
        }

        public string PerSecondLabel { get; set; }
        public string TotalLabel { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
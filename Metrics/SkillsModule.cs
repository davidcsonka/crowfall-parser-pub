﻿using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using Skills.Views;

namespace Skills
{
    public class SkillsModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            var regionManager = containerProvider.Resolve<IRegionManager>();
            regionManager.RegisterViewWithRegion("ContentRegion", typeof(Metrics));
            regionManager.RegisterViewWithRegion("DamageSkills", typeof(SkillList));
            regionManager.RegisterViewWithRegion("HealSkills", typeof(SkillList));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
        }
    }
}
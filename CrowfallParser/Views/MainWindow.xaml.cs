﻿using System;
using System.Reactive.Linq;
using System.Windows.Input;

namespace CrowfallParser.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            MouseDown += (sender, args) =>
            {
                if (args.ChangedButton == MouseButton.Left)
                    DragMove();
            };

            Loaded += (sender, args) =>
            {
                Top = Options.Options.Inst.Top;
                Left = Options.Options.Inst.Left;
            };

            Observable.FromEventPattern<EventHandler, EventArgs>(
                    h => LocationChanged += h,
                    h => LocationChanged -= h
                ).Sample(TimeSpan.FromSeconds(3))
                .ObserveOnDispatcher()
                .Subscribe(next =>
                {
                    Options.Options.Inst.Top = Top;
                    Options.Options.Inst.Left = Left;
                });
        }
    }
}